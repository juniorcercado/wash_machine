<?php

if($_SESSION["id_cargo"] == 2  ){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}

?>
<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar usuarios
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Administrar usuarios</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarUsuario">
          
          Agregar usuario

        </button>

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>foto</th>
           <th>nombre y apelllidos</th>
           <th>Documento</th>
           <th>cargo</th>
           <th>celular</th>
           <th>direccion</th>
           <th>distrito</th>
           <th>Provincia</th>
           <th>Ultmo Login</th>
            
           <th>Acciones</th>

         </tr> 

        </thead>

        <tbody>

        <?php

        $item = null;
        $valor = null;

        $usuarios = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

       foreach ($usuarios as $key => $value){
         
          echo ' <tr>
                  <td>'.($key+1).'</td>';

                  if($value["imagen"] != ""){

                    echo '<td><img src="'.$value["imagen"].'" class="img-thumbnail" width="40px"></td>';

                  }else{

                    echo '<td><img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail" width="40px"></td>';

                  }

          echo '<td>'.$value["nombre"]." ".$value["apellidos"].'</td>' ;
          echo '<td>'.$value["tipo_doc"].": ".$value["num_doc"].'</td>' ;

                 

                  echo '<td>'.$value["cargo"].'</td>';
                   echo '<td>'.$value["celular"].'</td>';
                    echo '<td>'.$value["direccion"].'</td>';
                     echo '<td>'.$value["distrito"].'</td>';
                     echo '<td>'.$value["provincia"].'</td>';



                             

                  echo '<td>'.$value["ultimo_login"].'</td>
                  <td>

                    <div class="btn-group">
                        
                      <button class="btn btn-warning btnEditarUsuario" idUsuario="'.$value["id"].'" data-toggle="modal" data-target="#modalEditarUsuario"><i class="fa fa-pencil"></i></button>

                      <button class="btn btn-danger btnEliminarUsuario" idUsuario="'.$value["id"].'" fotoUsuario="'.$value["imagen"].'" usuario="'.$value["email"].'"><i class="fa fa-times"></i></button>

                    </div>  

                  </td>

                </tr>';
        }


        ?> 

        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL AGREGAR USUARIO
======================================-->

<div id="modalAgregarUsuario" class="modal fade" role="dialog">
  
  <div class="modal-dialog modal_grande ">

    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Agregar usuario</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <div class="row">

              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                <!-- ENTRADA PARA EL NOMBRE -->
                
                <div class="form-group">
                  <label  >NOMBRE</label> 
                  <div class="input-group">
                    
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nombres" required>

                  </div>

                </div>

              </div>

              <!-- ENTRADA PARA EL APELLIDO -->
               <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                
                <div class="form-group">
                  <label  >APELLIDOS</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar apellidos" required>

                  </div>

                </div>

              </div>

               <!-- ENTRADA PARA EL SEXO -->
               <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                
                <div class="form-group">
                  <label  >SEXO</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon input-lg"><i class="fa fa-user"></i></span> 
                     
                    <div class="row">

                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                        <input type="radio" id="male" name="gender" value="male" class="radio_buton_grande">
                        <label for="male" class="margin_sexo">Varon</label><br>
                      </div>

                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                        <input type="radio" id="female" name="gender" value="female" class="radio_buton_grande">
                        <label for="female" class="margin_sexo">Mujer</label><br>
                      </div>

                    </div>
                  </div>

                </div>

              </div>
              <!-- ENTRADA PARA EL TIPO DOC -->
               <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                
                <div class="form-group">
                  <label  >TIPO DOC</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                    <select id="cars" class="form-control input-lg">
                      <option  >TIPO DOC.</option>
                      <option value="dni">DNI</option>
                      <option value="ruc">RUC</option>
                      <option value="audi">CARNET DE EXTRANJERIA</option>
                      <option value="audi">PASAPORTE</option>
                    </select>

                  </div>

                </div>

              </div>

            </div>

            <!-- -------------------------SEGUNDA FILA DEL FORMULARIO--------------------------------------- -->

            <div class="row">

              <!-- ENTRADA PARA EL DNI -->

              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                <div class="form-group">
                  <label  >DOCUMENTO</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-key"></i></span> 

                    <input type="number" class="form-control input-lg" name="nuevoUsuario" placeholder="Ingresar DNI" id="nuevoUsuario" required>

                  </div>

                </div>

              </div>

               <!-- ENTRADA PARA EL CORREO -->

              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                <div class="form-group">
                  <label  >CORREO</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-key"></i></span> 

                    <input type="text" class="form-control input-lg" name="nuevoUsuario" placeholder="Ingresar CORREO" id="nuevoUsuario" required>

                  </div>

                </div>

              </div>

              <!-- ENTRADA PARA LA CONTRASEÑA -->
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                <div class="form-group">
                  <label  >CONTRASEÑA</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span> 

                    <input type="password" class="form-control input-lg" name="nuevoPassword" placeholder="Ingresar contraseña" required>

                  </div>

                </div>

              </div>
               <!-- ENTRADA PARA LA CELULAR -->
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                <div class="form-group">
                  <label  >CELUAR</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span> 

                    <input type="text" class="form-control input-lg" name="nuevoPassword" placeholder="Ingresar CELULAR" required>

                  </div>

                </div>

              </div>

            </div> 

            <!-- -------------------------TERCERA FILA DEL FORMULARIO--------------------------------------- -->

            <div class="row"> 
             
              <!-- ENTRADA PARA SELECCIONAR SU PERFIL -->
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                 <div class="form-group">
                    <label  >CARGO</label> 
                    <div class="input-group">
                    
                      <span class="input-group-addon"><i class="fa fa-users"></i></span> 

                      <select class="form-control input-lg" name="nuevoPerfil">
                        
                        <option value="">Selecionar perfil</option>

                        <option value="Administrador">Administrador</option>

                        <option value="Especial">Especial</option>

                        <option value="Vendedor">Vendedor</option>

                      </select>

                    </div>
                  </div>
                </div>

                <!-- ENTRADA PARA LA TIPO PERSONA -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                  <div class="form-group">
                    <label >TIPO PERSONA</label> 
                    <div class="input-group">
                    
                      <span class="input-group-addon"><i class="fa fa-lock"></i></span> 

                      <select class="form-control input-lg" name="nuevoPerfil">
                          
                          <option value="">Tipo Persona</option>

                          <option value="Administrador">Natural</option>

                          <option value="Especial">Juridica</option>

                          

                        </select>

                    </div>

                  </div>

                </div>

                <!-- ENTRADA PARA EL DISTRITO -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                  <div class="form-group">
                    <label  >DISTRITO</label> 
                    <div class="input-group">
                    
                      <span class="input-group-addon"><i class="fa fa-lock"></i></span> 

                      <select class="form-control input-lg" name="nuevoPerfil">
                          
                          <option value="">DISTRITO</option>

                          <option value="Administrador">Natural</option>

                          <option value="Especial">Juridica</option>

                          

                        </select>

                    </div>

                  </div>

                </div>
                <!-- ENTRADA PARA EL PROVINCIA -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                  <div class="form-group">
                    <label  >PROVINCIA</label> 
                    <div class="input-group">
                    
                      <span class="input-group-addon"><i class="fa fa-lock"></i></span> 

                      <select class="form-control input-lg" name="nuevoPerfil">
                          
                          <option value="">PROVINCIA</option>

                          <option value="Administrador">Natural</option>

                          <option value="Especial">Juridica</option>

                          

                        </select>

                    </div>

                  </div>

                </div>

            </div>

           <!-- -------------------------CUARTA FILA DEL FORMULARIO--------------------------------------- -->

            <div class="row"> 
              
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <!-- ENTRADA PARA SUBIR FOTO -->

                 <div class="form-group">
                  
                  <label  >SUBIR FOTO</label> 

                  <input type="file" class="nuevaFoto" name="nuevaFoto">

                  <p class="help-block">Peso máximo de la foto 2MB</p>

                  <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail previsualizar" width="100px">

                </div>

              </div>

              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
              </div>

              <!-- ENTRADA PARA  DEPARTAMENTO -->
              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">

                <div class="form-group">
                  <label  >DEPARTAMENTO</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span> 

                    <select class="form-control input-lg" name="nuevoPerfil">
                        
                        <option value="">DEPARTAMENTO</option>

                        <option value="Administrador">Natural</option>

                        <option value="Especial">Juridica</option>

                        

                      </select>

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar usuario</button>

        </div>

        <?php

          $crearUsuario = new ControladorUsuarios();
          $crearUsuario -> ctrCrearUsuario();

        ?>

      </form>

    </div>

  </div>

</div>

<!--=====================================
MODAL EDITAR USUARIO
======================================-->

<div id="modalEditarUsuario" class="modal fade" role="dialog">
  
  <div class="modal-dialog modal_grande">

    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Editar usuario</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">
            
            <div class="row">

              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                <!-- ENTRADA PARA EL NOMBRE -->
                
                <div class="form-group">
                  <label  >NOMBRE</label> 
                  <div class="input-group">
                    
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nombres" required id="editarNombre">

                  </div>

                </div>

              </div>

              <!-- ENTRADA PARA EL APELLIDO -->
               <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                
                <div class="form-group">
                  <label  >APELLIDOS</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar apellidos" required id="editarApellidos">

                  </div>

                </div>

              </div>

               <!-- ENTRADA PARA EL SEXO -->
               <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                
                <div class="form-group">
                  <label  >SEXO</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon input-lg"><i class="fa fa-user"></i></span> 
                     
                    <div class="row">
                       
                       <script >
                        var hola = $(this).attr("#editarsexo").html(respuesta["nombre"]);
                       document.write(hola);
                       </script>

                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                        <input type="radio" id="editarsexo" name="gender" value="male" class="radio_buton_grande" required>
                        <label for="male" class="margin_sexo">Varon</label><br>
                      </div>

                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                        <input type="radio" id="female" name="gender" value="female" class="radio_buton_grande">
                        <label for="female" class="margin_sexo">Mujer</label><br>
                      </div>

                    </div>
                  </div>

                </div>

              </div>
              <!-- ENTRADA PARA EL TIPO DOC -->
               <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                
                <div class="form-group">
                  <label  >TIPO DOC</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                    <select id="cars" class="form-control input-lg">
                      <option value="" id="editarTipodoc"></option>
                      <option value="dni">DNI</option>
                      <option value="ruc">RUC</option>
                      <option value="audi">CARNET DE EXTRANJERIA</option>
                      <option value="audi">PASAPORTE</option>
                    </select>

                  </div>

                </div>

              </div>

            </div>

            <!-- -------------------------SEGUNDA FILA DEL FORMULARIO--------------------------------------- -->

            <div class="row">

              <!-- ENTRADA PARA EL DNI -->

              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                <div class="form-group">
                  <label  >DOCUMENTO</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-key"></i></span> 

                    <input type="number" class="form-control input-lg" name="nuevoUsuario" placeholder="Ingresar DNI"  required id="editarNum_doc">

                  </div>

                </div>

              </div>

               <!-- ENTRADA PARA EL CORREO -->

              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                <div class="form-group">
                  <label  >CORREO</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-key"></i></span> 

                    <input type="text" class="form-control input-lg" name="nuevoUsuario" placeholder="Ingresar CORREO"  required id="editarCorreo">

                  </div>

                </div>

              </div>

              <!-- ENTRADA PARA LA CONTRASEÑA -->
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                <div class="form-group">
                  <label  >CONTRASEÑA</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span> 

                    <input type="password" class="form-control input-lg" name="nuevoPassword" placeholder="Editar nueva contraseña" required>

                  </div>

                </div>

              </div>
               <!-- ENTRADA PARA LA CELULAR -->
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                <div class="form-group">
                  <label  >CELUAR</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span> 

                    <input type="number" class="form-control input-lg" name="nuevoPassword" placeholder="Ingresar CELULAR" required id="editarCelular">

                  </div>

                </div>

              </div>

            </div> 

            <!-- -------------------------TERCERA FILA DEL FORMULARIO--------------------------------------- -->

            <div class="row"> 
             
              <!-- ENTRADA PARA SELECCIONAR SU PERFIL -->
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                 <div class="form-group">
                    <label  >CARGO</label> 
                    <div class="input-group">
                    
                      <span class="input-group-addon"><i class="fa fa-users"></i></span> 

                      <select class="form-control input-lg" name="nuevoPerfil">
                        
                        <option value="" id="editarCargo"></option>

                        <option value="Administrador">Administrador</option>

                        <option value="Especial">Especial</option>

                        <option value="Vendedor">Vendedor</option>

                      </select>

                    </div>
                  </div>
                </div>

                <!-- ENTRADA PARA LA TIPO PERSONA -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                  <div class="form-group">
                    <label >TIPO PERSONA</label> 
                    <div class="input-group">
                    
                      <span class="input-group-addon"><i class="fa fa-lock"></i></span> 

                      <select class="form-control input-lg" name="nuevoPerfil">
                          
                          <option value="" id="editarTipo_persona"> </option>

                          <option value="Administrador">Natural</option>

                          <option value="Especial">Juridica</option>

                          

                        </select>

                    </div>

                  </div>

                </div>

                <!-- ENTRADA PARA EL DISTRITO -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                  <div class="form-group">
                    <label  >DISTRITO</label> 
                    <div class="input-group">
                    
                      <span class="input-group-addon"><i class="fa fa-lock"></i></span> 

                      <select class="form-control input-lg" name="nuevoPerfil">
                          
                          <option value="" id="editarDistrito"> </option>

                          <option value="Administrador">Natural</option>

                          <option value="Especial">Juridica</option>

                          

                        </select>

                    </div>

                  </div>

                </div>
                <!-- ENTRADA PARA EL PROVINCIA -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">

                  <div class="form-group">
                    <label  >PROVINCIA</label> 
                    <div class="input-group">
                    
                      <span class="input-group-addon"><i class="fa fa-lock"></i></span> 

                      <select class="form-control input-lg" name="nuevoPerfil">
                          
                          <option value="" id="editarProvincia"> </option>

                          <option value="Administrador">Natural</option>

                          <option value="Especial">Juridica</option>

                          

                        </select>

                    </div>

                  </div>

                </div>

            </div>

           <!-- -------------------------CUARTA FILA DEL FORMULARIO--------------------------------------- -->

            <div class="row"> 
              
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">

                <!-- ENTRADA PARA SUBIR FOTO -->

                <div class="form-group">
                
                  <div class="panel">SUBIR FOTO</div>

                  <input type="file" class="nuevaFoto" name="editarFoto">

                  <p class="help-block">Peso máximo de la foto 2MB</p>

                  <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail previsualizarEditar" width="100px">

                  <input type="hidden" name="fotoActual" id="fotoActual">

                </div>

              </div>

              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
              </div>

              <!-- ENTRADA PARA  DEPARTAMENTO -->
              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">

                <div class="form-group">
                  <label  >DEPARTAMENTO</label> 
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span> 

                    <select class="form-control input-lg" name="nuevoPerfil">
                        
                        <option value="" id="editarDepartamento" >DEPARTAMENTO</option>

                        <option value="Administrador">Natural</option>

                        <option value="Especial">Juridica</option>

                        

                      </select>

                  </div>

                </div>

              </div>

            </div>
  



          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Modificar usuario</button>

        </div>

     <?php

          $editarUsuario = new ControladorUsuarios();
          $editarUsuario -> ctrEditarUsuario();

        ?> 

      </form>

    </div>

  </div>

</div>

<?php

  $borrarUsuario = new ControladorUsuarios();
  $borrarUsuario -> ctrBorrarUsuario();

?> 


