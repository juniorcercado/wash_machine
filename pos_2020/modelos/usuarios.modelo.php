<?php

require_once "conexion.php";

class ModeloUsuarios{

	/*=============================================
	MOSTRAR USUARIOS
	=============================================*/
	 

	static public function mdlMostrarUsuarios($tabla, $item, $valor){

		if($item != null){
			$cargo= "cargo";
			$distrito= "distrito";
			$distrito= "tipo_doc";

			$stmt = Conexion::conectar()->prepare("SELECT t.id as id,t.nombre as nombre, t.apellidos as apellidos,t.id_sexo as id_sexo,t.id_tipo_doc as id_tipo_doc,t.num_doc as num_doc,t.email as email,t.password as password,t.id_cargo as id_cargo,t.id_tipo_persona as id_tipo_persona,t.id_distrito as id_distrito,t.fecha as fecha,t.estado as estado,t.celular as celular, t.direccion as direccion,t.imagen as imagen,t.ultimo_login as ultimo_login,c.nombre as cargo,d.nombre as distrito, ti.nombre as tipo_doc, p.nombre as provincia, tp.nombre as tipo_persona, dp.nombre as departamento
				FROM $tabla t,$cargo c, distrito d,provincia p, tipo_doc ti,tipo_persona tp,departamento dp
				WHERE t.$item = :$item and t.id_cargo = c.id and t.id_distrito = d.id and t.id_tipo_doc = ti.id
									 and d.id_provincia =p.id and p.id_departamento = dp.id " );

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$cargo= "cargo";
			$distrito= "distrito";
			$distrito= "tipo_doc";

			$stmt = Conexion::conectar()->prepare("SELECT t.id as id,t.nombre as nombre, t.apellidos as apellidos,t.id_sexo as id_sexo,t.id_tipo_doc as id_tipo_doc,t.num_doc as num_doc,t.email as email,t.password as password,t.id_cargo as id_cargo,t.id_tipo_persona as id_tipo_persona,t.id_distrito as id_distrito,t.fecha as fecha,t.estado as estado,t.celular as celular, t.direccion as direccion,t.imagen as imagen,t.ultimo_login as ultimo_login,c.nombre as cargo,d.nombre as distrito, ti.nombre as tipo_doc, p.nombre as provincia
				FROM $tabla t ,$cargo c, distrito d,provincia p, tipo_doc ti 
				WHERE t.id_cargo = c.id and t.id_distrito = d.id and t.id_tipo_doc = ti.id and d.id_provincia =p.id and t.id >=0");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}
		

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	REGISTRO DE USUARIO
	=============================================*/

	static public function mdlIngresarUsuario($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre, usuario, password, perfil, foto) VALUES (:nombre, :usuario, :password, :perfil, :foto)");

		$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":usuario", $datos["usuario"], PDO::PARAM_STR);
		$stmt->bindParam(":password", $datos["password"], PDO::PARAM_STR);
		$stmt->bindParam(":perfil", $datos["perfil"], PDO::PARAM_STR);
		$stmt->bindParam(":foto", $datos["foto"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";	

		}else{

			return "error";
		
		}

		$stmt->close();
		
		$stmt = null;

	}

	/*=============================================
	EDITAR USUARIO
	=============================================*/

	static public function mdlEditarUsuario($tabla, $datos){
	
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre, password = :password, perfil = :perfil, foto = :foto WHERE usuario = :usuario");

		$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":password", $datos["password"], PDO::PARAM_STR);
		$stmt -> bindParam(":perfil", $datos["perfil"], PDO::PARAM_STR);
		$stmt -> bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
		$stmt -> bindParam(":usuario", $datos["usuario"], PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR USUARIO
	=============================================*/

	static public function mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE $item2 = :$item2");

		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	BORRAR USUARIO
	=============================================*/

	static public function mdlBorrarUsuario($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;


	}

}